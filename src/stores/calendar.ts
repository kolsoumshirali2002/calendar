import { defineStore } from 'pinia'
import { ref, reactive } from 'vue'
interface PersonEvent {
  start: string
  end: string
  title: string
  content: string
  contentFull: string
  class: string
  id: number
}
export const useCalanderStore = defineStore('calander', {
  state: () => ({
    editMode: ref(false),
    locale: ref('en'),
    events: reactive([
      {
        start: '2023-4-10 10:00',
        end: '2023-4-10 13:00',
        title: 'playing Golf with brother',
        content: 'Click to see event detail',
        contentFull:
          'i should provide myself for this play, because this play is so much important for me also i love playing with family such as brother',
        class: 'or-bg',
        id: 1
      },
      {
        start: '2023-4-12 1:00:00',
        end: '2023-4-12 5:00',
        title: 'Need to go shopping',
        content: 'Click to see event detail',
        contentFull:
          'My shopping list is rather long : 1)Avocados 2)Tomatoes 3)Potatoes 4)Mangoes 5)cake 6)ice cream',
        class: 'mg-bg',
        id: 2
      }
    ]),
    personEvent: reactive({
      caption: '',
      description: '',
      start: '',
      end: '',
      backEventColor: 'rd-bg',
      id: 0
    })
  }),
  actions: {
    async changeLocale(payload: string) {
      this.locale = await payload
    },
    addEvent(payload: PersonEvent) {
      this.events.push(payload)
    },
    async observeEvent(payload: number) {
      const indexValue = await this.events.findIndex((item, index) => {
        return item.id === payload
      })
      this.personEvent.caption = this.events[indexValue].title
      this.personEvent.description = this.events[indexValue].contentFull
      this.personEvent.start = this.events[indexValue].start
      this.personEvent.end = this.events[indexValue].end
      this.personEvent.backEventColor = this.events[indexValue].class
      this.personEvent.id = this.events[indexValue].id
    },
    emptyPersonEvent() {
      this.personEvent.caption = ''
      this.personEvent.description = ''
      this.personEvent.start = ''
      this.personEvent.end = ''
      this.personEvent.backEventColor = 'rd-bg'
      this.personEvent.id = 0
    },
    async removeEvent() {
      const indexValue = await this.events.findIndex((item, index) => {
        return item.id === this.personEvent.id
      })
      this.events.splice(indexValue, 1)
    },
    async editEvent() {
      const indexValue = await this.events.findIndex((item, index) => {
        return item.id === this.personEvent.id
      })
      await this.events.splice(indexValue, 1, {
        start: this.personEvent.start,
        end: this.personEvent.end,
        title: this.personEvent.caption,
        content: 'Click to see event detail',
        contentFull: this.personEvent.description,
        class: this.personEvent.backEventColor,
        id: this.personEvent.id
      })
    }
  }
})
