import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import VueCal from 'vue-cal'
import i18n from './i18n'
import VueDatePicker from '@vuepic/vue-datepicker';
import '@/assets/scss/main.scss'
import '@/assets/css/main.css'

const app = createApp(App)
app.use(createPinia())
app.use(router)
app.use(i18n)
app.component('VueDatePicker', VueDatePicker);
app.component('VueCal', VueCal);
app.mount('#app')
